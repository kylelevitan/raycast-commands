#!/usr/bin/env python2
# -*- coding: UTF-8 -*-

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Harvest Hours
# @raycast.mode fullOutput

# Optional parameters:
# @raycast.icon ⏱

# Documentation:
# @raycast.author Kyle Levitan

import json
import requests
import datetime

harvest_id = "132122"
today = datetime.date.today()
last_monday = today - datetime.timedelta(days=today.weekday())
coming_friday = today + datetime.timedelta((4-today.weekday()) % 7)

url = "https://api.harvestapp.com/v2/reports/time/projects?from={}&to={}".format(last_monday,coming_friday)

headers = {
  "Harvest-Account-ID" : harvest_id,
  "Authorization" : "Bearer 2482490.pt.glfXGlKY6cMX326MbNZXlJssJlfnTjAOeeC_i465iOJKy58NLSBgJdZ3rX7xXMpM_Akt_82hIN-2zNTWo0wNTA",
  "User-Agent" : "Harvest API Example",
}

response = requests.get(url, headers=headers)
payload = json.loads(response.text)
total = 0

for (k, v) in payload.items():
  if k == 'results':
    for item in v:
      client = item['client_name']
      hours = str(item['total_hours'])
      total = total + item['total_hours']
      print(client + '.....' + hours)

print('\rTotal: ' + str(total) + ' hours for the week of ' + str(last_monday))
